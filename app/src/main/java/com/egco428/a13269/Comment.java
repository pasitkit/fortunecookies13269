package com.egco428.a13269;

/**
 * Created by Mos pc on 8/11/2559.
 */
public class Comment {
    private String message;
    private String date;
    private String image;

    private long id;
    public long getId(){return id;}
    public void setId(long id){this.id = id;}

    public Comment(long id, String imgname, String message, String timestamp){
        this.id = id;
        this.message = message;
        this.date = timestamp;
        this.image = imgname;
    }

    public String getMessage(){return message;}
    public String getTimestamp(){return date;}
    public String getImgname(){return image;}
}
