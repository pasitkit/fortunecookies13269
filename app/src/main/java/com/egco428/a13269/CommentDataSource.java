package com.egco428.a13269;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mos pc on 8/11/2559.
 */
public class CommentDataSource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbhelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_IMAGE, MySQLiteHelper.COLUMN_COMMENT, MySQLiteHelper.COLUMN_DATE};

    public CommentDataSource(Context context){
        dbhelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLiteAbortException {
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        dbhelper.close();
    }

    public Comment createMessage(String imgname, String message, String timestamp){
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_IMAGE, imgname);
        values.put(MySQLiteHelper.COLUMN_COMMENT, message);
        values.put(MySQLiteHelper.COLUMN_DATE, timestamp);
        open();
        long insertID = database.insert(MySQLiteHelper.TABLE_RESULTS, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_RESULTS, allColumns, MySQLiteHelper.COLUMN_ID + "=" + insertID, null, null, null, null);
        cursor.moveToFirst();
        Comment newMessage = cursorToComment(cursor);
        cursor.close();
        return newMessage;
    }

    public void deleteFortuneResult(Comment results){
        long id = results.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_RESULTS, MySQLiteHelper.COLUMN_ID + "=" + id,null);
    }

    public List<Comment> getAllComments(){
        List<Comment> results = new ArrayList<Comment>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_RESULTS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Comment newresult = cursorToComment(cursor);
            results.add(newresult);
            cursor.moveToNext();
        }
        cursor.close();
        return results;
    }

    public Comment cursorToComment(Cursor cursor){ //set value to comment
        Comment results = new Comment(cursor.getLong(0),cursor.getString(1),cursor.getString(2),cursor.getString(3));
        return results;
    }
}
