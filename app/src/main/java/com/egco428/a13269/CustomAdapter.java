package com.egco428.a13269;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mos pc on 8/11/2559.
 */
public class CustomAdapter extends ArrayAdapter<Comment> {
    Context context;
    List<Comment> objects;

    public CustomAdapter(Context context, int resource, List<Comment> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Comment comment = objects.get(position);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE); //link with interface
        View view = inflater.inflate(R.layout.row,null);

        TextView txt = (TextView)view.findViewById(R.id.resultfortune);
        txt.setText(comment.getMessage());

        TextView txtDT = (TextView)view.findViewById(R.id.datetimeview);
        txtDT.setText(comment.getTimestamp());

        ImageView image = (ImageView)view.findViewById(R.id.imagecoo);
        int res = context.getResources().getIdentifier(comment.getImgname(),"drawable",context.getPackageName());
        image.setImageResource(res);
        if(comment.getImgname().equals("opened_cookie_2")|| comment.getImgname().equals("opened_cookie_4")){
            txt.setTextColor(Color.parseColor("#ff3300"));
        }
        else{
            txt.setTextColor(Color.parseColor("#4d94ff"));
        }
        return view;
    }

}

